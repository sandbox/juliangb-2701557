; Standard declarations.
core = 7.x
api = 2

; Module dependencies.
; Required for basic installation.
projects[] = pm

; Recommended modules.
; Not required, but enhance functionality.
projects[] = masquerade

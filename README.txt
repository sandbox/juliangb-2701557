PM Demo is a tool for site admins considering using, or setting up Drupal PM.

PM Demo adds the ability to create demo roles, users and content. All objects
created can also be uninstalled through this module, even after real objects
have been built up around them.

To install or uninstall demo objects, visit the PM Demo settings page at
admin/config/pm/demo.

<?php
/**
 * @file
 * Functions relating to demo roles.
 */

/**
 * Installs demo roles.
 */
function pmdemo_install_roles() {
  $roles = pmdemo_define_roles();

  foreach ($roles as $role_name => $permissions) {

    if (!user_role_load_by_name($role_name)) {
      $role = new stdClass();
      $role->name = $role_name;
      user_role_save($role);
    }

    // @todo Assign permissions.
    // Need to check that a permission exists to avoid errors.
  }

  variable_set('pmdemo_roles_installed', TRUE);
}

/**
 * Uninstall demo roles.
 */
function pmdemo_uninstall_roles() {
  $roles = pmdemo_define_roles();

  foreach ($roles as $role_name) {
    user_role_delete($role_name);
  }

  variable_del('pmdemo_roles_installed');
}

/**
 * Defines demo roles.
 */
function pmdemo_define_roles() {
  return array(
    'PM Admin' => array(
      'Project Management: access dashboard',
      'Project Management: access administration pages',
    ),
    'PM Director' => array(
      'Project Management: access dashboard',
    ),
    'PM Manager' => array(
      'Project Management: access dashboard',
    ),
    'PM Developer' => array(
      'Project Management: access dashboard',
    ),
    'PM Support Agent' => array(
      'Project Management: access dashboard',
    ),
    'PM Client' => array(
      'Project Management: access dashboard',
    ),
  );
}

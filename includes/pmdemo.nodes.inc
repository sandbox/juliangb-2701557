<?php
/**
 * @file
 * Functions relating to demo nodes.
 */

/**
 * Installs demo nodes.
 */
function pmdemo_install_nodes() {
  $nodes = pmdemo_define_nodes();
  $nids = array();

  foreach ($nodes as &$node) {
    // @todo Check next lines.
    // Only attempt to create the node if the content type exists.
    // Thus pmdemo can handle missing modules.
    if (type_exists($node['type'])) {
      $node['parent_nid'] = _pmdemo_get_parent_nid($nodes, $node['parent']);
      node_save($node);

      $nids[] = $node->nid;
    }
  }

  variable_set('pmdemo_nodes_nids', $nids);
  variable_set('pmdemo_nodes_installed', TRUE);
}

/**
 * Uninstall demo nodes.
 */
function pmdemo_uninstall_nodes() {
  $nids = variable_get('pmdemo_nodes_nids', array());

  foreach ($nids as $nid) {
    // @todo Check next line
    node_delete($nid);
  }

  variable_del('pmdemo_nodes_nids');
  variable_del('pmdemo_nodes_installed');
}

/**
 * Defines demo nodes.
 */
function pmdemo_define_nodes() {
  // Parent references are to the parent's key in this array.
  return array(
    'pmorganization-1' => array(
      'title' => 'Website client',
      'type' => 'pmorganization',
    ),
    'pmproject-1' => array(
      'title' => 'New website build',
      'type' => 'pmproject',
      'parent' => 'pmorganization-1',
    ),
    'pmtask-1' => array(
      'title' => 'Install Drupal',
      'type' => 'pmtask',
      'parent' => 'pmproject-1',
    ),
  );
}

/**
 * Determines the parent nid for a new node.
 *
 * This is used so that pmdemo can skip creating some nodes if the relevant
 * modules are not installed without breaking the parent hierarchy.
 */
function _pmdemo_get_parent_nid(&$nodes, $parent_key) {
  // Parent nid determined from immediate parent.
  if (isset($nodes[$parent_key]['nid'])) {
    return $nodes[$parent_key]['nid'];
  }
  // Try finding the parent nid from a previous parent.
  elseif (isset($nodes[$parent_key]['parent'])) {
    return _pmdemo_get_parent_nid($nodes, $parent_key);
  }
  // No parent is defined.
  else {
    return NULL;
  }
}

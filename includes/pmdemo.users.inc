<?php
/**
 * @file
 * Functions relating to demo users.
 */

/**
 * Installs demo users.
 */
function pmdemo_install_users() {
  $users = pmdemo_define_users();

  foreach ($users as $user_name => $roles) {
    // Basic user details.
    $user = array(
      'name' => $user_name,
      'mail' => $user_name . '@example.com',
      'init' => $user_name . '@example.com',
      'status' => 1,
      'roles' => array(),
    );
    
    // Role membership.
    foreach ($roles as $role_name) {
      $role_id = user_role_load_by_name($role_name);
      $user['roles'][$role_id] = TRUE;
    }
    
    user_save(NULL, $user);
  }

  variable_set('pmdemo_users_installed', TRUE);
}

/**
 * Uninstall demo users.
 */
function pmdemo_uninstall_users() {
  $users = pmdemo_define_users();

  foreach ($users as $user_name) {
    // @todo Check next line
    user_delete($user_name);
  }

  variable_del('pmdemo_users_installed');
}

/**
 * Defines demo users.
 */
function pmdemo_define_users() {
  return array(
    'PM Admin' => array('PM Admin'),
    'PM Director' => array('PM Director'),
    'PM Manager' => array('PM Manager'),
    'PM Developer' => array('PM Developer'),
    'PM Support Agent' => array('PM Support Agent'),
    'PM Client' => array('PM Client'),
  );
}
